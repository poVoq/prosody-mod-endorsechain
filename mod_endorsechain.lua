-- LUALOCALS < ---------------------------------------------------------
local pairs, require
    = pairs, require
-- LUALOCALS > ---------------------------------------------------------

local jidutil = require("util.jid")
local jidsplit = jidutil.split

local usermgr = require("core.usermanager")
local is_admin = usermgr.is_admin

local rostermgr = require("core.rostermanager")
local load_roster = rostermgr.load_roster

local timer = require("util.timer")

------------------------------------------------------------------------
-- Calculate Endorsement Path

-- Get the list of people subscribing to a user, including those with
-- open pending requests.  This is effectively the list of people who are
-- "endorsing" this user as a member of the community.
local function subscribers(user, host)
 	local parents = {}
	local rost = load_roster(user, host) or {}
	for k, v in pairs(rost) do
		local u, h = jidsplit(k)
		if h == host and (v.subscription == "to" or v.subscription == "both") then
			parents[u] = true
		end
	end
	if rost.pending then
		for k, v in pairs(rost.pending) do
			local u, h = jidsplit(k)
			if h == host then
				parents[jidsplit(k)] = true
			end
		end
	end
	return parents
end

-- Get the list of users blocked by this user.  This blocks a user from
-- "endorsement," and also blocks any downstream endorsees from endorsing
-- affected users.
local function getblocked(user, host)
	local privacy_lists = datamanager.load(user, host, "privacy") or {}
	local default_list_name = privacy_lists.default
	if not default_list_name then return {} end
	local default_list = privacy_lists.lists[default_list_name]
	if not default_list then return {} end
	local items = default_list.items
	local names = {}
	for i=1, #items do
		local item = items[i]
		if item.type == "jid" and item.action == "deny" then
			local u, h = jidsplit(item.value)
			if h == host then
				names[u] = true
			end
		end
	end
	return names
end

-- Add a user to the top of an endorsement path.
local function pathcat(t, add)
	local x = {}
	if add ~= nil then x[1] = add end
	for i = 1, #t do x[#x + 1] = t[i] end
	return x
end

-- Calculate the quickest unbroken path of endorsements from an admin to
-- a user, or return nil if no valid path can be found.
local function pathresolve(user, host)
	local q = {{u = user, p = {user}}}
	while #q > 0 do
		local batch = q
		q = {}
		for _, item in pairs(batch) do
			local jid = item.u .. "@" .. host

			local blocked = getblocked(item.u, host)
			local isblocked
			local inpath = {}
			for _, u in ipairs(item.p) do
				isblocked = isblocked or blocked[u]
				inpath[u] = true
			end
			if not isblocked then
				if is_admin(jid, host) then
					return item.p
				end
				for u, _ in pairs(subscribers(item.u, host)) do
					if not inpath[u] then
						q[#q + 1] = {u = u, p = pathcat(item.p, u)}
					end
				end
			end
		end
	end
end

------------------------------------------------------------------------
-- Login Hook

-- When a user logs in, ensure that the user has a chain of endorsement.
module:hook("authentication-success", function(evt)
	local session = evt.session
	if not session then return end

	local user = session.username
	local host = session.host
	if not user or not host then return end

	if not pathresolve(user, host) then
		session:close({condition = "policy-violation",
			text = "No valid chain of endorsement."})
	end
end)

------------------------------------------------------------------------
-- Endorsement Removal Hooks

local function recheckuser(user, host, seen)
	-- Only check each user once per traversal.
	local jid = user .. "@" .. host
	seen = seen or {}
	if seen[jid] then return end
	seen[jid] = true

	-- If the user is still valid, we're done here.
	if pathresolve(user, host) then return end

	-- Kick off user, if online.
	local h = hosts and hosts[host]
	for un, u in pairs(h and h.sessions or {}) do
		if un == user then
			for r, s in pairs(u and u.sessions or {}) do
				s:close({condition = "policy-violation",
					text = "Chain of endorsement broken."})
			end
		end
	end

	-- Revalidate users that may have been depending on this one
	-- for chain of endorsement.
	local rost = load_roster(user, host) or {}
	for k, v in pairs(rost) do
		local u, h = jidsplit(k)
		if h == host and (v.ask or v.subscription == "from" or v.subscription == "both") then
			recheckuser(u, host, seen)
		end
	end
end

-- Recheck endorsements that may have been lost when a block is added.
module:hook("iq/self/urn:xmpp:blocking:block", function(evt)
	local stanza = evt.stanza
	if not stanza or not stanza.attr or stanza.attr.type ~= "set" then return end
	local block = stanza.tags and stanza.tags[1]
	if not block or block.name ~= "block" then return end
	for t in block:childtags() do
		if t and t.attr and t.attr.jid then
			local u, h = jidsplit(t.attr.jid)
			timer.add_task(0, function()
				recheckuser(u, h)
			end)
		end
	end
end, 10000)

-- Recheck endorsements that may have been lost when a subscription is removed.
module:hook("iq/self/jabber:iq:roster:query", function(evt)
	local stanza = evt.stanza
	if not stanza or not stanza.attr or stanza.attr.type ~= "set" then return end
	local query = stanza.tags and stanza.tags[1]
	if not query or #query.tags ~= 1 then return end
	local item = query.tags[1]
	if item.name ~= "item" or not item.attr or item.attr.xmlns ~= "jabber:iq:roster"
		or not item.attr.jid then return end
	local u, h = jidsplit(item.attr.jid)
	timer.add_task(0, function()
		recheckuser(u, h)
	end)
end, 10000)
